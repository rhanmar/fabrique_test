# Notification Service
## Test task *Fabrique*

### Installation and run
* clone this repo
* create `.env` file and write down:
  * FB_TOKEN=<YOUR_TOKEN>
* `make build` or `docker-compose build`
* `make up` or `docker-compose up`


### Endpoints
* `mailings/`
  * Filters: *filter_type*, *filter_value*
* `mailings/statistics/`
* `mailings/{pk}/statistics_detail/`
* `messages/`
  * Filters: *mailing*, *client*, *status*
* `clients/`
  * Filters: *operator_code*, *tag*


### Commands Makefile
* `make build`: docker-compose build 
* `make up`: docker-compose up
* `make down`: docker-compose down
* `make attach`: attach to web container (example: for ipdb)
* `make test`: run tests
* `make make_migrations`: make migrations
* `make migrate`: migrate
* `make show_migrations`: list of migrations
* `make show_urls`: list of urls
* `make exec_web`: exec to web
* `make exec_db`: exec to db
* `make create_superuser`: create superuser
* `make shell`: go to shell
* `make worker`: run celery worker

### What is done
* Testing
* docker-compose
* Swagger (`'docs/'`)
* Logging (partially)
* Admin page (`'admin/'`)


### How to rebuild the service
To rebuild service:  
* `sudo chown -R $USER data`
* `make build`
