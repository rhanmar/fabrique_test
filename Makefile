build:
	docker-compose build

up:
	docker-compose up

down:
	docker-compose down

attach:
	docker attach fabrique_test_web_1

test:
	docker exec -it fabrique_test_web_1 python ./manage.py test

make_migrations:
	docker exec -it fabrique_test_web_1 python ./manage.py makemigrations

migrate:
	docker exec -it fabrique_test_web_1 python ./manage.py migrate

show_migrations:
	docker exec -it fabrique_test_web_1 python ./manage.py showmigrations

show_urls:
	docker exec -it fabrique_test_web_1 python ./manage.py show_urls

exec_web:
	docker exec -it fabrique_test_web_1 /bin/bash

exec_db:
	docker exec -it fabrique_test_db_1 /bin/bash

create_superuser:
	docker exec -it fabrique_test_web_1 python ./manage.py createsuperuser

shell:
	docker exec -it fabrique_test_web_1 python ./manage.py shell

worker:
	docker exec -it fabrique_test_web_1 celery -A fabrique_test worker --beat -l info
