import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fabrique_test.settings")

app = Celery("fabrique_test", broker='redis://redis:6379')

app.config_from_object("django.conf:settings", namespace="CELERY")


app.conf.beat_schedule = {
    "start_sending_mailings": {
        "task": "notifications.tasks.send_mailings",
        "schedule": crontab(minute='*/1'),
    }
}

app.autodiscover_tasks()
