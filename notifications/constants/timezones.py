import pytz

timezones1 = pytz.all_timezones
timezones2 = pytz.all_timezones

TIMEZONE_CHOICES = list(zip(timezones1, timezones2))
