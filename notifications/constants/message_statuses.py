from django.utils.translation import gettext as _


class MessageStatuses:
    """Enum that contains choices for Mailing.filter_type."""
    SENT = "SENT"
    ERROR = "ERROR"
    ACTIVE = "ACTIVE"

    RESOLVER = {
        SENT: _("SENT"),
        ERROR: _("ERROR"),
        ACTIVE: _("ACTIVE"),
    }

    CHOICES = RESOLVER.items()
