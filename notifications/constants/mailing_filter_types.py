from django.utils.translation import gettext as _


class MailingFilterTypes:
    """Enum that contains choices for Mailing.filter_type."""
    CODE = "CODE"
    TAG = "TAG"

    RESOLVER = {
        CODE: _("Mobile Operator Code"),
        TAG: _("Client Tag"),
    }

    CHOICES = RESOLVER.items()
