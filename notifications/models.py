from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import gettext as _

from notifications.constants import (mailing_filter_types, message_statuses,
                                     timezones)
from notifications.utils.phone_validators import (does_contain_only_digits,
                                                  does_start_with_seven,
                                                  is_length_equal_to_eleven)
from notifications.utils.tokens_generator import create_auth_token

post_save.connect(create_auth_token, sender=User)


class Mailing(models.Model):
    """Model that contains info about Notification Mailings."""

    start_time = models.DateTimeField(
        verbose_name=_("Mailing Start Time"),
        null=True,
    )
    end_time = models.DateTimeField(
        verbose_name=_("Mailing Deadline"),
        null=True,
        blank=True,
    )
    text = models.TextField(
        verbose_name=_("Mailing Text"),
        null=False,
        blank=False,
    )
    filter_type = models.CharField(
        verbose_name=_("Mailing Filter Type"),
        max_length=64,
        null=False,
        blank=False,
        choices=mailing_filter_types.MailingFilterTypes.CHOICES
    )
    filter_value = models.CharField(
        verbose_name=_("Mailing Filter Value"),
        max_length=128,
        null=False,
        blank=False,
    )
    is_processed = models.BooleanField(
        verbose_name=_("Is Mailing Was Processed"),
        default=False,
    )

    def __str__(self) -> str:
        return f"Mailing. FilterType: {self.filter_type}. FilterValue: {self.filter_value} Text: {self.text}"

    class Meta:
        verbose_name = _("Mailing")
        verbose_name_plural = _("Mailings")


class Client(models.Model):
    """Model that contains info about Notification Clients."""

    phone = models.CharField(
        max_length=11,
        verbose_name=_("Client's Phone Number"),
        validators=[is_length_equal_to_eleven, does_contain_only_digits, does_start_with_seven],
        help_text=_("Phone number must contain digits only, begin with 7 and have length 11 characters or be empty.")
    )
    operator_code = models.CharField(
        verbose_name=_("Mobile Operator Code"),
        max_length=128,
        null=True,
        blank=True,
    )
    tag = models.CharField(
        verbose_name=_("Client's Tag"),
        max_length=128,
        null=True,
        blank=True,
    )
    timezone = models.CharField(
        verbose_name=_("Client's Timezone"),
        max_length=64,
        null=True,
        blank=True,
        choices=timezones.TIMEZONE_CHOICES,
    )

    def __str__(self) -> str:
        return f"Client. Phone: {self.phone}, OperatorCode: {self.operator_code}, Tag: {self.tag}"

    class Meta:
        verbose_name = _("Client")
        verbose_name_plural = _("Clients")


class Message(models.Model):
    """Model that contains info about Notification Messages."""

    created_at = models.DateTimeField(
        verbose_name=_("Time when message was created"),
        auto_now_add=True,
    )
    sent_at = models.DateTimeField(
        verbose_name=_("Time when message was sent"),
        null=True,
        blank=True,
    )
    status = models.CharField(
        verbose_name=_("Message Status"),
        max_length=64,
        null=False,
        blank=False,
        choices=message_statuses.MessageStatuses.CHOICES,
    )
    mailing = models.ForeignKey(
        verbose_name=_("Message Mailing"),
        to="notifications.Mailing",
        related_name="messages",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )
    client = models.ForeignKey(
        verbose_name=_("Message Client"),
        to="notifications.Client",
        related_name="messages",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
    )

    def __str__(self) -> str:
        return f"Message. Status: {self.status}, Mailing: {self.mailing}, Client: {self.client}"

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")
