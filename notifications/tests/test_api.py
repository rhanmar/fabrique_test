from django.contrib.auth.models import User
from django.db.models import Case, Count, When
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from notifications.constants.mailing_filter_types import MailingFilterTypes
from notifications.constants.message_statuses import MessageStatuses
from notifications.models import Mailing
from notifications.serializers import MailingStatisticsSerializer
from notifications.tests.factories import (ClientFactory, MailingFactory,
                                           MessageFactory)


class MailingAPITestCase(APITestCase):
    """TestCase for Mailing API."""

    def setUp(self) -> None:
        MailingFactory(
            filter_type=MailingFilterTypes.CODE,
            filter_value="duplicated"
        )
        MailingFactory(
            filter_type=MailingFilterTypes.TAG,
            filter_value="duplicated"
        )
        MailingFactory(
            filter_type=MailingFilterTypes.CODE,
            filter_value="test_code"
        )
        MailingFactory(
            filter_type=MailingFilterTypes.TAG,
            filter_value="test_tag"
        )

        user = User.objects.create(username='zxc')
        self.client.force_login(user)
        self.url = reverse("mailing-list")

    def test_filter(self):
        """Test filter fields (filter_type, filter_value) in Mailing model."""
        # duplicates
        url = self.url + "?filter_value=duplicated"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + "?filter_value=duplicated&filter_type=CODE"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            1
        )

        url = self.url + "?filter_value=duplicated&filter_type=TAG"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            1
        )

        # different
        url = self.url + "?filter_type=TAG"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + "?filter_type=CODE"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + "?filter_type=CODE&filter_value=test_code"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            1
        )

        url = self.url + "?filter_type=TAG&filter_value=test_tag"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            1
        )

        url = self.url + "?filter_type=TAG&filter_value=test_code"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            0
        )

        url = self.url + "?filter_type=CODE&filter_value=test_tag"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            0
        )


class MessageAPITestCase(APITestCase):
    """TestCase for Message API."""

    def setUp(self) -> None:

        client1 = ClientFactory()
        self.client1 = client1
        client2 = ClientFactory()
        mailing1 = MailingFactory()
        mailing2 = MailingFactory()
        self.mailing2 = mailing2
        MessageFactory(client=client1, mailing=mailing1, status=MessageStatuses.SENT)
        MessageFactory(client=client1, mailing=mailing1, status=MessageStatuses.SENT)
        MessageFactory(client=client1, mailing=mailing1, status=MessageStatuses.ERROR)
        MessageFactory(client=client2, mailing=mailing2, status=MessageStatuses.ERROR)
        MessageFactory(client=client2, mailing=mailing2, status=MessageStatuses.ERROR)
        MessageFactory(client=client2, mailing=mailing2, status=MessageStatuses.SENT)
        MessageFactory(client=client1, mailing=mailing2, status=MessageStatuses.ACTIVE)
        MessageFactory(client=client1, mailing=mailing2, status=MessageStatuses.ACTIVE)

        user = User.objects.create(username='zxc')
        self.client.force_login(user)
        self.url = reverse("message-list")

    def test_filter(self):
        """Test filter fields (status, client, mailing) in Message model."""
        url = self.url + "?status=SENT"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            3
        )

        url = self.url + "?status=ACTIVE"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + f"?status=SENT&client={self.client1.pk}"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + f"?status=SENT&client={self.client1.pk}"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + f"?client={self.client1.pk}&mailing={self.mailing2.pk}"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            2
        )

        url = self.url + f"?mailing={self.mailing2.pk}"
        response = self.client.get(url)
        self.assertEqual(
            len(response.json()),
            5
        )


class ClientAPITestCase(APITestCase):
    """TestCase for Client API."""

    def setUp(self) -> None:
        user = User.objects.create(username='zxc')
        self.client.force_login(user)
        self.url = reverse("client-list")

    def test_unauthorized(self):
        """Test unauthorized user."""
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_wrong_phone(self):
        """Test correct phone number in request."""
        data = {
            "phone": "89832021212"
        }
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["phone"][0].__str__(),
            "The phone must start with 7."
        )

        data = {
            "phone": "7(983)-202 12 12"
        }
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["phone"][0].__str__(),
            "The phone's length must be 11."
        )

        data = {
            "phone": "798320512QW"
        }
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["phone"][0].__str__(),
            "The phone must contain digits only."
        )

        data = {
            "phone": "798"
        }
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["phone"][0].__str__(),
            "The phone's length must be 11."
        )

        data = {
            "phone": ""
        }
        response = self.client.post(self.url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["phone"][0].__str__(),
            "This field may not be blank."
        )


class MailingStatisticsAPITestCase(APITestCase):
    """TestCase for Mailing Statistics API."""

    def setUp(self) -> None:
        mailing = MailingFactory()
        client = ClientFactory()
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        query = Mailing.objects.all().prefetch_related("messages").\
            annotate(messages_count=Count("messages")).\
            annotate(sent_messages=Count(Case(When(messages__status='SENT', then=1)))).\
            annotate(active_messages=Count(Case(When(messages__status='ACTIVE', then=1)))).\
            annotate(failed_messages=Count(Case(When(messages__status='ERROR', then=1))))
        self.mailing_statistics = MailingStatisticsSerializer(query, many=True).data

        user = User.objects.create(username='zxc')
        self.client.force_login(user)
        self.url = reverse("mailing-statistics")
        self.response = self.client.get(self.url)

    def test_active_messages_count(self):
        """Test 'active_messages' count."""
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.response.json()[0]["active_messages"], 5)

    def test_sent_messages_count(self):
        """Test 'sent_messages' count."""
        self.assertEqual(self.response.json()[0]["sent_messages"], 3)

    def test_failed_messages_count(self):
        """Test 'failed_messages' count."""
        self.assertEqual(self.response.json()[0]["failed_messages"], 4)

    def test_messages_total_count(self):
        """Test 'messages_count' count."""
        self.assertEqual(self.response.json()[0]["messages_count"], 12)
