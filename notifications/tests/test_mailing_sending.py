import json
from datetime import timedelta
from unittest.mock import patch

import requests
from django.test import TestCase
from django.utils.timezone import now
from rest_framework.test import APITestCase

from notifications.models import Message
from notifications.tasks import send_mailings
from notifications.tests.factories import ClientFactory, MailingFactory
from notifications.utils.model_getters import (get_clients_for_mailing,
                                               get_mailings_for_sending)


class SendingMailingsTestCase(APITestCase):
    """TestCase for Senging Mailings."""

    def setUp(self) -> None:
        ClientFactory(operator_code="44", tag="TT1")
        ClientFactory(operator_code="44", tag="TT2")
        self.mailing = MailingFactory(
            start_time=now() - timedelta(days=1),
            filter_type="CODE",
            filter_value="44"
        )

    def test_sending_ok(self):
        """Test 'send_mailings' works correct."""
        with patch("requests.post") as mock_post:
            response_data = {
                "code": 0,
                "message": "OK",
            }
            response = requests.models.Response()
            response.status_code = 200
            response._content = json.dumps(response_data).encode("utf-8")
            mock_post.return_value = response

            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "SENT")
            self.assertEqual(sent_messages[1].status, "SENT")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)

    def test_sending_not_200_v1(self):
        """Test 'send_mailings' works correct with bad response."""
        with patch("requests.post") as mock_post:
            response_data = {
                "code": -1,
                "message": "base64",
            }
            response = requests.models.Response()
            response.status_code = 200
            response._content = json.dumps(response_data).encode("utf-8")
            mock_post.return_value = response

            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "ERROR")
            self.assertEqual(sent_messages[1].status, "ERROR")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)

    def test_sending_not_200_v2(self):
        """Test 'send_mailings' works correct with bad response."""
        with patch("requests.post") as mock_post:
            response_data = {
                "code": 403,
                "message": "message",
            }
            response = requests.models.Response()
            response.status_code = 200
            response._content = json.dumps(response_data).encode("utf-8")
            mock_post.return_value = response

            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "ERROR")
            self.assertEqual(sent_messages[1].status, "ERROR")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)

    def test_sending_not_200_v3(self):
        """Test 'send_mailings' works correct with bad response."""
        with patch("requests.post") as mock_post:
            response_data = {
                "code": 0,
                "message": "message",
            }
            response = requests.models.Response()
            response.status_code = 405
            response._content = json.dumps(response_data).encode("utf-8")
            mock_post.return_value = response

            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "ERROR")
            self.assertEqual(sent_messages[1].status, "ERROR")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)

    def test_sending_connection_error(self):
        """Test 'send_mailings' works correct with 'ConnectionError' exception."""
        with patch("requests.post") as mock_post:
            mock_post.side_effect = requests.exceptions.ConnectionError
            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "ERROR")
            self.assertEqual(sent_messages[1].status, "ERROR")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)

    def test_sending_timeout(self):
        """Test 'send_mailings' works correct with 'ConnectTimeout' exception."""
        with patch("requests.post") as mock_post:
            mock_post.side_effect = requests.exceptions.ConnectTimeout
            send_mailings()
            sent_messages = Message.objects.all()
            self.assertEqual(sent_messages.count(), 2)
            self.assertEqual(sent_messages[0].status, "ERROR")
            self.assertEqual(sent_messages[1].status, "ERROR")

            self.mailing.refresh_from_db()
            self.assertTrue(self.mailing.is_processed)


class ModelGettersTestCase(TestCase):
    """TestCase for Model Getters."""

    def test_client_getter(self):
        """Test 'get_clients_for_mailing' by 'filter_type' and 'filter_value'."""
        ClientFactory(operator_code="44", tag="TT1")
        ClientFactory(operator_code="44", tag="TT2")
        ClientFactory(operator_code="33", tag="TT1")

        self.assertEqual(get_clients_for_mailing(filter_type="TAG", filter_value="TT1").count(), 2)
        self.assertEqual(get_clients_for_mailing(filter_type="TAG", filter_value="TT2").count(), 1)
        self.assertEqual(get_clients_for_mailing(filter_type="TAG", filter_value="TT0").count(), 0)
        self.assertEqual(get_clients_for_mailing(filter_type="CODE", filter_value="44").count(), 2)
        self.assertEqual(get_clients_for_mailing(filter_type="CODE", filter_value="01").count(), 0)

    def test_mailings_getter(self):
        """Test 'get_mailings_for_sending'."""
        MailingFactory(start_time=now()-timedelta(days=1),)
        MailingFactory(start_time=now()-timedelta(days=1),)
        MailingFactory(start_time=now()-timedelta(days=1),)
        MailingFactory(start_time=now()-timedelta(days=1),)
        self.assertEqual(get_mailings_for_sending().count(), 4)

        MailingFactory(start_time=now()-timedelta(days=1), end_time=now()+timedelta(days=1))
        MailingFactory(start_time=now()-timedelta(days=1), end_time=now()+timedelta(days=1))
        self.assertEqual(get_mailings_for_sending().count(), 6)

        MailingFactory(start_time=now()-timedelta(days=2), end_time=now()-timedelta(days=1))
        MailingFactory(start_time=now()-timedelta(days=2), end_time=now()-timedelta(days=1))
        self.assertEqual(get_mailings_for_sending().count(), 6)
        self.assertNotEqual(get_mailings_for_sending().count(), 8)

        MailingFactory(start_time=now()-timedelta(days=1), is_processed=True)
        self.assertEqual(get_mailings_for_sending().count(), 6)
        self.assertNotEqual(get_mailings_for_sending().count(), 9)
        MailingFactory(start_time=now()-timedelta(days=1), end_time=now()+timedelta(days=1), is_processed=True)
        MailingFactory(start_time=now()-timedelta(days=2), end_time=now()-timedelta(days=1), is_processed=True)
        self.assertEqual(get_mailings_for_sending().count(), 6)
        self.assertNotEqual(get_mailings_for_sending().count(), 11)
