from django.db.models import Case, Count, When
from django.test import TestCase

from notifications.constants.message_statuses import MessageStatuses
from notifications.models import Mailing
from notifications.serializers import MailingStatisticsSerializer
from notifications.tests.factories import (ClientFactory, MailingFactory,
                                           MessageFactory)


class MailingStatisticsSerializerTestCase(TestCase):
    """TestCase for Mailing Statistics Serializer."""

    def setUp(self) -> None:
        mailing = MailingFactory()
        client = ClientFactory()
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.SENT)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ERROR)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        MessageFactory(mailing=mailing, client=client, status=MessageStatuses.ACTIVE)
        query = (
            Mailing.objects.all().prefetch_related("messages").
            annotate(messages_count=Count("messages")).
            annotate(sent_messages=Count(Case(When(messages__status='SENT', then=1)))).
            annotate(active_messages=Count(Case(When(messages__status='ACTIVE', then=1)))).
            annotate(failed_messages=Count(Case(When(messages__status='ERROR', then=1))))
        )
        self.mailing_statistics = MailingStatisticsSerializer(query, many=True).data

    def test_active_messages_count(self):
        """Test 'active_messages' count."""
        data = self.mailing_statistics
        self.assertEqual(data[0]["active_messages"], 4)

    def test_sent_messages_count(self):
        """Test 'sent_messages' count."""
        data = self.mailing_statistics
        self.assertEqual(data[0]["sent_messages"], 3)

    def test_failed_messages_count(self):
        """Test 'failed_messages' count."""
        data = self.mailing_statistics
        self.assertEqual(data[0]["failed_messages"], 2)

    def test_messages_total_count(self):
        """Test 'messages_count' count."""
        data = self.mailing_statistics
        self.assertEqual(data[0]["messages_count"], 9)
