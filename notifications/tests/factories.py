import factory
from django.utils.timezone import now

from notifications.constants.mailing_filter_types import MailingFilterTypes
from notifications.constants.message_statuses import MessageStatuses
from notifications.utils.phones_generator import get_correct_phone


class MailingFactory(factory.django.DjangoModelFactory):
    """Factory for Mailing model."""

    start_time = now()
    text = factory.Faker("text")
    filter_type = MailingFilterTypes.CODE
    filter_value = factory.Faker("word")

    class Meta:
        model = "notifications.Mailing"


class ClientFactory(factory.django.DjangoModelFactory):
    """Factory for Client model."""

    phone = factory.LazyFunction(get_correct_phone)

    class Meta:
        model = "notifications.Client"


class MessageFactory(factory.django.DjangoModelFactory):
    """Factory for Message model."""

    status = MessageStatuses.SENT
    mailing = factory.SubFactory("notifications.tests.factories.MailingFactory")
    client = factory.SubFactory("notifications.tests.factories.ClientFactory")

    class Meta:
        model = "notifications.Message"
