from django_filters import rest_framework as filters

from notifications.models import Mailing, Message


class MailingFilter(filters.FilterSet):
    """Filter for Mailing model.

    Filterable fields:
    - filter_type
    - filter_value
    """

    filter_type = filters.CharFilter(field_name="filter_type")
    filter_value = filters.CharFilter(field_name="filter_value")

    class Meta:
        model = Mailing
        fields = [
            "filter_type",
            "filter_value"
        ]


class MessageFilter(filters.FilterSet):
    """Filter for Message model.

    Filterable fields:
    - mailing
    - client
    - status
    """

    mailing = filters.CharFilter(field_name="mailing__pk")
    client = filters.CharFilter(field_name="client__pk")
    status = filters.CharFilter(field_name="status")

    class Meta:
        model = Message
        fields = [
            "mailing",
            "client",
            "status",
        ]
