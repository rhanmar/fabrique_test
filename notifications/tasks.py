import os

import requests
from celery.utils.log import get_task_logger
from django.utils.timezone import now
from rest_framework import status

from fabrique_test.celery import app
from notifications.constants.message_statuses import MessageStatuses
from notifications.constants.probe_server_url import PROBE_SERVER_URL
from notifications.models import Message
from notifications.utils.model_getters import (get_clients_for_mailing,
                                               get_mailings_for_sending)


@app.task
def send_mailings() -> None:
    """Start to send mailings."""
    logger = get_task_logger('info')

    print(f"Log: Start sending.")
    logger.info(f"Log: Start sending.")

    token = os.environ.get('FB_TOKEN')
    mailings_to_send = get_mailings_for_sending()

    print(f"Log: There are {len(mailings_to_send)} Mailings for sending.")
    logger.info(f"Log: There are {len(mailings_to_send)} Mailings for sending.")

    for mailing in mailings_to_send:
        print(f"Log: Mailing {mailing.pk} is processing.")
        logger.info(f"Log: Mailing {mailing.pk} is processing.")
        mailing.is_processed = True
        mailing.save()
        clients = get_clients_for_mailing(mailing.filter_type, mailing.filter_value)
        print(f"Log: There are {len(clients)} clients for Mailing {mailing.pk}.")
        logger.info(f"Log: There are {len(clients)} clients for Mailing {mailing.pk}.")

        for client in clients:
            message = Message.objects.create(
                status=MessageStatuses.ACTIVE,
                mailing=mailing,
                client=client,
            )
            data = {
                "id": message.pk,
                "phone": int(client.phone),
                "text": mailing.text,
            }

            try:
                response = requests.post(
                    PROBE_SERVER_URL.format(message.pk),
                    headers={"Authorization": f"Bearer {token}"},
                    json=data,
                )
                print(f"Log: Response for Message {message.pk} is {response.status_code}.")
                logger.info(f"Log: Response for Message {message.pk} is {response.status_code}.")
                if all([
                    response.status_code == status.HTTP_200_OK,
                    response.json()["code"] == 0,
                    response.json()["message"] == "OK",
                ]):
                    print(f"Log: Message {message.pk} gets status {MessageStatuses.SENT}.")
                    logger.info(f"Log: Message {message.pk} gets status {MessageStatuses.SENT}.")
                    message.status = MessageStatuses.SENT
                    message.sent_at = now()
                    message.save()
                else:
                    print(f"Log: Message {message.pk} gets status {MessageStatuses.ERROR}.")
                    logger.info(f"Log: Message {message.pk} gets status {MessageStatuses.ERROR}.")
                    message.status = MessageStatuses.ERROR
                    message.save()

            except (requests.ConnectionError, requests.ConnectTimeout) as error:
                print(f"Log: Error occurred while Message {message.pk} sending.")
                print(f"Log: {error.__doc__}.")
                print(f"Log: Message {message.pk} gets status {MessageStatuses.ERROR}.")
                logger.info(f"Log: Error occurred while Message {message.pk} sending.")
                logger.info(f"Log: {error.__doc__}.")
                logger.info(f"Log: Message {message.pk} gets status {MessageStatuses.ERROR}.")
                message.status = MessageStatuses.ERROR
                message.save()
