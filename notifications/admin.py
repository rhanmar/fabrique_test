from django.contrib import admin

from notifications.models import Client, Mailing, Message


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    """Admin for Mailing model."""

    list_display = (
        "start_time",
        "end_time",
        "text",
        "filter_type",
        "filter_value",
        "is_processed",
    )


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    """Admin for Client model."""

    list_display = (
        "phone",
        "operator_code",
        "tag",
        "timezone",
    )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """Admin for Message model."""

    list_display = (
        "created_at",
        "status",
        "mailing",
        "client",
    )
