from rest_framework import routers

from notifications.viewsets import (ClientViewSet, MailingViewSet,
                                    MessageViewSet)

router = routers.DefaultRouter()
router.register("clients", ClientViewSet)
router.register("mailings", MailingViewSet)
router.register("messages", MessageViewSet)

urlpatterns = router.urls
