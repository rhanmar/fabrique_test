from rest_framework import serializers

from notifications.models import Client, Mailing, Message


class ClientSerializer(serializers.ModelSerializer):
    """Serializer for Client model."""
    class Meta:
        model = Client
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
    """Serializer for Mailing model."""
    class Meta:
        model = Mailing
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    """Serializer for Message model."""

    class Meta:
        model = Message
        fields = (
            "status",
            "mailing",
            "client",
            "sent_at",
        )


class MailingStatisticsSerializer(serializers.ModelSerializer):
    """Serializer for Mailing statistics.

    Extra fields:
    - sent_messages: messages that were successfully sent;
    - failed_messages: messages that got error while sending;
    - active_messages: messages that are going to be sent;
    - sent_messages: total number of messages for current Mailing;
    """

    sent_messages = serializers.IntegerField(read_only=True)
    active_messages = serializers.IntegerField(read_only=True)
    failed_messages = serializers.IntegerField(read_only=True)
    messages_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Mailing
        fields = (
            "pk",
            "start_time",
            "end_time",
            "filter_type",
            "filter_value",
            "text",
            "messages_count",
            "sent_messages",
            "active_messages",
            "failed_messages",
        )
