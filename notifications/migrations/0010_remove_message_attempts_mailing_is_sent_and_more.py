# Generated by Django 4.0.3 on 2022-03-31 09:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0009_message_sent_at_alter_message_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='attempts',
        ),
        migrations.AddField(
            model_name='mailing',
            name='is_sent',
            field=models.BooleanField(default=False, verbose_name='Is Mailing Sent'),
        ),
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('SENT', 'SENT'), ('ERROR', 'ERROR'), ('ACTIVE', 'ACTIVE'), ('EXPIRED', 'EXPIRED')], max_length=64, verbose_name='Message Status'),
        ),
    ]
