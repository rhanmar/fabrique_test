# Generated by Django 4.0.3 on 2022-03-30 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0006_message'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': 'Client', 'verbose_name_plural': 'Clients'},
        ),
        migrations.AlterModelOptions(
            name='mailing',
            options={'verbose_name': 'Mailing', 'verbose_name_plural': 'Mailings'},
        ),
        migrations.AlterModelOptions(
            name='message',
            options={'verbose_name': 'Message', 'verbose_name_plural': 'Messages'},
        ),
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.CharField(choices=[('SENT', 'SENT'), ('ERROR', 'ERROR')], max_length=64, verbose_name='Message Status'),
        ),
    ]
