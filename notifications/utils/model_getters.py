from django.db.models import QuerySet
from django.utils.timezone import now

from notifications.constants.mailing_filter_types import MailingFilterTypes
from notifications.models import Client, Mailing


def get_clients_for_mailing(filter_type: str, filter_value: str) -> QuerySet:
    """Return Clients for Mailing by 'filter_type' and 'filter_value'."""
    if filter_type == MailingFilterTypes.CODE:
        clients = Client.objects.filter(operator_code=filter_value)
    else:  # Means filter_type == MailingFilterTypes.TAG:
        clients = Client.objects.filter(tag=filter_value)
    return clients


def get_mailings_for_sending() -> QuerySet:
    """Return Mailings that can be sent (were not processed before and have correct 'start_time' and 'end_time')."""
    return Mailing.objects.filter(is_processed=False, start_time__lte=now()).exclude(end_time__lte=now(),)
