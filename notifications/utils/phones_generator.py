import random


def get_correct_phone() -> str:
    """Generate correct phone."""
    phone_start = ['7']
    phone_main = [str(random.randrange(10)) for _ in range(10)]
    phone = phone_start + phone_main
    return "".join(phone)
