from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


def does_start_with_seven(phone: str) -> None:
    """Check if phone starts with 7."""
    if not phone[0] == '7':
        raise ValidationError(
            _("The phone must start with 7."),
        )


def is_length_equal_to_eleven(phone: str) -> None:
    """Check if phone contains 11 characters."""
    if not len(phone) == 11:
        raise ValidationError(
            _("The phone's length must be 11."),
        )


def does_contain_only_digits(phone: str) -> None:
    """Check if phone starts contains only digits."""
    for item in phone:
        if not item.isdigit():
            raise ValidationError(
                _("The phone must contain digits only.")
            )