from django.db.models import Case, Count, When
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from notifications.filters import MailingFilter, MessageFilter
from notifications.models import Client, Mailing, Message
from notifications.serializers import (ClientSerializer, MailingSerializer,
                                       MessageSerializer)

from .serializers import MailingStatisticsSerializer


class ClientViewSet(ModelViewSet):
    """ViewSet to process requests for Client model."""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["operator_code", "tag"]


class MailingViewSet(ModelViewSet):
    """ViewSet to process requests for Mailing model."""
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [DjangoFilterBackend]
    filterset_class = MailingFilter

    @action(detail=False, methods=["get"])
    def statistics(self, request, *args, **kwargs):
        """Action to get statistics about all Mailing."""
        query = (
            Mailing.objects.all().prefetch_related("messages").
            annotate(messages_count=Count("messages")).
            annotate(sent_messages=Count(Case(When(messages__status='SENT', then=1)))).
            annotate(active_messages=Count(Case(When(messages__status='ACTIVE', then=1)))).
            annotate(failed_messages=Count(Case(When(messages__status='ERROR', then=1))))
        )
        result = MailingStatisticsSerializer(query, many=True).data
        return Response(data=result, status=status.HTTP_200_OK)

    @action(detail=True, methods=["get"])
    def statistics_detail(self, request, *args, **kwargs):
        """Action to get statistics for specific Mailing."""
        query = (
            Mailing.objects.filter(pk=kwargs["pk"]).prefetch_related("messages").
            annotate(messages_count=Count("messages")).
            annotate(sent_messages=Count(Case(When(messages__status='SENT', then=1)))).
            annotate(active_messages=Count(Case(When(messages__status='ACTIVE', then=1)))).
            annotate(failed_messages=Count(Case(When(messages__status='ERROR', then=1))))
        )
        if not query.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data="No such Mailing.")
        result = MailingStatisticsSerializer(query.first()).data
        return Response(data=result, status=status.HTTP_200_OK)


class MessageViewSet(ModelViewSet):
    """ViewSet to process requests for Message model."""
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [DjangoFilterBackend]
    filterset_class = MessageFilter
